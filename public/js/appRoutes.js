angular.module('appRoutes', []).config(function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'HomeController',
			resolve: { isHomePage: function() { return true } }
		})

		.when('/login', {
			templateUrl: 'views/user/login.html',
			controller: 'UserController'
		})

		.when('/signup', {
			templateUrl: 'views/user/signup.html',
			controller: 'UserController'
		})
		.when('/account', {
			templateUrl: 'views/user/profile.html',
			controller: 'UserProfileController',
			resolve: {
				loginCheck: checkLoggedin
			}
		})
		.when('/campaigns', {
			templateUrl: 'views/campaigns/index.html',
			controller: 'CampaignsController',
			resolve: {
				loginCheck: checkLoggedin
			}
		}).when('/campaigns/new', {
			templateUrl: 'views/campaigns/new.html',
			controller: 'NewCampaignController',
			controllerAs: 'campaign',
			resolve: {
				loginCheck: checkLoggedin
			}
		}).when('/campaigns/:id/', {
			templateUrl: 'views/campaigns/edit.html',
			controller: 'CampaignsController',
			controllerAs: 'campaign',
			resolve: {
				loginCheck: checkLoggedin
			}
		}).otherwise({
      redirectTo: '/'
    });

	$locationProvider.html5Mode(true);

});


var checkLoggedin = function($q, $timeout, $http, $location, $rootScope) {
    var deferred = $q.defer();

    $http.get('/loggedin').then(function(res) {
		console.log(res);
		$rootScope.errorMessage = null;
		//User is Authenticated
		if (res.data !== null) {
			$rootScope.currentUser = res.data;
			deferred.resolve();
		} else { //User is not Authenticated
			$rootScope.errorMessage = 'Please login to continue.';
			deferred.reject();
			$location.url('/login');
		}
    });
    return deferred.promise;
}
