angular.module('UserService', []).factory('User', ['$http', '$rootScope', '$window', function($http, $rootScope,$window) {
  var User = {};

  User.checkLoggedin = function($q, $timeout, $http, $location, $rootScope) {
    var deferred = $q.defer();

    $http.get('/loggedin').success(function(user) {
      $rootScope.errorMessage = null;
      //User is Authenticated
      if (user !== '0') {
        $rootScope.currentUser = user;
        deferred.resolve();
      } else { //User is not Authenticated
        $rootScope.errorMessage = 'Please login to continue.';
        deferred.reject();
        $location.url('/login');
      }
    });
    return deferred.promise;
  }

  User.login = function(user) {
    return $http.post('/login', user).then(function(response) {
      $rootScope.currentUser = response.data
      return response.data;
    });
  }

  User.logout = function(){
    $rootScope.currentUser = null;
    $window.$localStorage.token = null;
    //return $http.get('/logout');
  }

  User.signup = function(user) {
    return $http.post("/signup", user);
  }

  User.update = function(user) {
    return $http.post("/account/update", user);
  };

  return User;

}]);
