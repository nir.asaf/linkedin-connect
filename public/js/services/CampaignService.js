angular.module('CampaignService', []).factory('Campaigns', ['$http', '$rootScope', function($http, $rootScope) {
  var Campaigns = {};

  Campaigns.new = function(campaign){
    return $http.post('/api/campaigns', campaign).then(function(response) {
      return response.data;
    });
  }

  return Campaigns;

}]);
