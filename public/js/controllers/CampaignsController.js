var m = angular.module('CampaignsCtrl', []);
m.controller('CampaignsController', function($scope, $rootScope, $http) {
  $rootScope.path = "campaigns";

  $scope.campaigns = [];

	$scope.getCampaigns = function() {
		$http.get("/api/campaigns").then(function(res){
			$scope.campaigns = res.data;
		});
	};

	$scope.getCampaigns();
});

m.controller('NewCampaignController', function($scope, $rootScope, $http,$location, Campaigns) {
  console.log("NewCampaignController");
  $scope.createCampaign = function() {
    Campaigns.new($scope.campaign).then(function(res){
      console.log(res);
      $location.url("/campaigns");
    });
  }
});
