var m = angular.module('UserCtrl', []);
m.controller('UserController', function($scope, $http, $window, $location, $rootScope, User) {
  $rootScope.path = "user";

  $scope.erros = []
  $scope.user = {};

  $scope.login = function() {
    User.login($scope.user).then(function(res) {
        console.log(res);
        if(res.message === "ok") {
          $window.localStorage.token = res.token;
          $location.url('/campaigns');
        }
        else {
          $scope.errorMessage = res.data.message;
        }
    }).catch(function(res){
        $scope.errorMessage = res;
    });
  }


});

m.controller('UserProfileController', function($scope, $http, User, $rootScope) {
  $rootScope.path = "userprofile";

  $scope.user_account = $rootScope.currentUser;
  $scope.savingStatus = "";

  $scope.saveAccount = function() {
      u = $scope.user_account;
      $scope.savingStatus = "Saving account...";

      User.update({
        first_name: u.first_name,
        last_name: u.last_name
      }).then(function(res){
        $scope.savingStatus = res.data.msg;
      }).catch(function(res){
        $scope.savingStatus = "Error occured while saving.";
      });    
  };

});

m.controller('SignupController', function($scope, $http, $location, User, $rootScope) {
    $rootScope.path = "signup";

    $scope.user = {}

    $scope.signup = function(){
        User.signup($scope.user).then(function(res) {
            console.log("created user: ", res.data);
            $location.url("/");
        }).catch(function(res){
            $scope.errorMessage = res.data.errors; 
        })
    };

});
