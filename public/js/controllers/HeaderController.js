angular.module('HeaderCtrl', []).controller('HeaderController', function($scope, $location, User, $rootScope) {

  $scope.logout = function() {
  	User.logout();
  	$location.url('/');
  }

});
