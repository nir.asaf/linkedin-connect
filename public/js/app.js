/**
*  The main application module
*/

app = angular.module('ConnectBot', [
	'ngRoute',
	'CampaignsCtrl',
	'DashboardCtrl',
	'HeaderCtrl',
	'HomeCtrl',
	'UserCtrl',
	'UserService',
	'CampaignService',
	'appRoutes'
])
.factory('AuthInterceptor', function ($rootScope, $q, $window) {
    $localStorage = $window.localStorage;

    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($localStorage.token) {
                config.headers.Authorization = 'JWT ' + $localStorage.token;
            }
            return config;
        },
        responseError: function(response) {
            if(response.status === 401 || response.status === 403) {
                $location.path('/login');
            }
            return $q.reject(response);
        }
    };
})
 
.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
})
.run(function($rootScope) {

});


