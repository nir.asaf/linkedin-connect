/**
 * Created by Oleg Vlasov on 06.07.17.
 */

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse){
    console.log('onMessage content_linkedin.js: ' + JSON.stringify(message));
    switch (message.action) {
        case 'has_next_page': sendResponse(hasNextPage()); break;
        case 'next_page': sendResponse(nextPage()); break;
        case 'get_profiles': sendResponse(getProfiles()); break;
        case 'send_connect':
            sendConnect(message.invite_message).then(result => { sendResponse(result); });
            return true;
    }
});

function timeout(ms) {
    return new Promise(resolve => {
        setTimeout(() => { resolve() }, ms);
    })
}

function hasNextPage() {
    let result = document.querySelectorAll("nav#pagination a.next-pagination.disabled");
    return (result.length === 0);
}

function nextPage() {
    document.querySelector("a.next-pagination").click();
    return window.location.href;
}

function getProfiles() {
    let links = document.querySelectorAll("ul#results-list li a.name-link.profile-link");
    let profiles = [];
    for(let p of links){
        profiles.push({title: p.title, url: p.href});
    }
    return profiles;
}

function clickConnectButton(timeout) {
    return new Promise(resolve => {
        document.querySelector("button.connect-button").click();
        setTimeout(() => { resolve() }, timeout);
    });
}

function writeConnectMessage(message, timeout) {
    return new Promise(resolve => {
        document.querySelector("textarea#connect-message-content").value = message;
        setTimeout(() => { resolve() }, timeout);
    });
}

function cancelForm(timeout) {
    return new Promise(resolve => {
        document.querySelector("form.connect-form button.cancel-button").click();
        setTimeout(() => { resolve() }, timeout);
    });
}

function submitForm(timeout) {
    return new Promise(resolve => {
        document.querySelector("form.connect-form button.submit-button").click();
        setTimeout(() => { resolve() }, timeout);
    });
}

function markSubmitFormButton(timeout) {
    return new Promise(resolve => {
        document.querySelector("form.connect-form button.submit-button").style.color = "red";
        setTimeout(() => { resolve() }, timeout);
    });
}

async function sendConnect(invite_message) {
    await clickConnectButton(2000);
    await writeConnectMessage(invite_message, 2000);
    await markSubmitFormButton(2000);
    return true;
}

console.log('(ConnectBot) LinkedIn detected');