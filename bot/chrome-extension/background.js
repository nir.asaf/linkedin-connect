/**
 * Created by Oleg Vlasov on 06.07.17.
 */

const FILTER = 'https://www.linkedin.com/sales/search?facet=G&facet=I&facet=CS&facet=SE&facet.G=us%3A84&facet.I=4&facet.I=96&facet.I=6&facet.CS=G&facet.CS=I&titleScope=CURRENT&jobTitleEntities=Vice%20President%20Of%20Software%20Development_4727&jobTitleEntities=Vice%20President%20Of%20Software%20Engineering_10174&jobTitleEntities=Vice%20President%20Quality%20Assurance_4664&jobTitleEntities=Software%20Quality%20Assurance%20Manager_3215&jobTitleEntities=Software%20Engineering%20Manager_1685&jobTitleEntities=VP%20Quality%20Assurance_4664&jobTitleEntities=VP%20Software%20Development_4727&jobTitleEntities=VP%20Software%20Engineering_10174&jobTitleEntities=CTO_153&jobTitleEntities=VP%20Engineering_520&jobTitleEntities=Vice%20President%20Software_5121&jobTitleEntities=Vice%20President%20Engineering_520&jobTitleEntities=Vice%20President%20Software%20Engineering_10174&jobTitleEntities=Director%20Of%20Engineering_326&jobTitleEntities=Director%20Of%20Software%20Development_1592&jobTitleEntities=Director%20Of%20Software_2053&jobTitleEntities=Director%20Of%20Quality%20Assurance_1045&facet.SE=7&facet.SE=6&count=25&start=875&updateHistory=true&searchHistoryId=1397653806&trackingInfoJson.contextId=4D3C003D8AE7BD1440E1A252492B0000';

let state_store = {
    connects: 0,
    working: false,
    stopping: false,
    campaignId: 0,
    online: false
};

let state = new Proxy(state_store, {
    set: function(target, prop, value) {
        target[prop] = value;
        chrome.runtime.sendMessage({state: target});
        return Reflect.set(target, prop, value);
    }
});

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {

    if (message.action) {
        switch (message.action) {

            case 'get_campaigns':
                getCampaigns().then((campaigns) => {
                    chrome.runtime.sendMessage({campaigns: campaigns, state: state});
                });
                break;

            case 'get_state':
                chrome.runtime.sendMessage({state: state});
                break;

            case 'set_campaign':
                state.campaignId = message.id;
                getConnects(state.campaignId).then((connects) => {
                   if (connects.length) {
                       state.connects = connects.length;
                   } else {
                       state.connects = 0;
                   }
                });
                break;

            case 'toggle':
                if (state.campaignId > 0) {
                    state.working = !state.working;
                    if (state.working) {
                        start(FILTER);
                    } else {
                        state.stopping = true;
                    }
                } else {
                    alert('Please, select Campaign');
                }
                break;
        }
    }
});

function createTab (url) {
    return new Promise(resolve => {
        chrome.tabs.create({ url: url }, tab => {
            chrome.tabs.onUpdated.addListener(function listener (tabId, info) {
                if (info.status === 'complete' && tabId === tab.id) {
                    chrome.tabs.onUpdated.removeListener(listener);
                    resolve(tab);
                }
            });
        });
    });
}

async function start(filter) {

    let filter_tab = await createTab(filter);

    let selected_campaign = await getCampaign(state.campaignId);
    let sent_connects = await getConnects(state.campaignId);

    //console.log('sent_connects ' + sent_connects);

    let sent_titles = [];

    if (sent_connects) {
        if (sent_connects.length > 0) {
            sent_connects.forEach((el, i) => {
                sent_titles.push(sent_connects[i].lead_title);
            });
        }
    }

    let n = 0;
    while (n < 5) {

        await timeout(3000);

        let profiles = await getProfiles(filter_tab);

        for (let profile of profiles) {

            if (sent_titles.indexOf(profile.title) > -1) {
                console.log('SKIP connect to ' + profile.title + ' (' + profile.url + ')');
                continue;
            }

            console.log('sending connect to ' + profile.title + ' (' + profile.url + ')...');

            await sendConnectScript(profile, selected_campaign.invite_message);

            state.connects++;
            if (state.stopping) break;
            await timeout(3000);
        }
        if (state.stopping) break;
        console.log('next page');
        if (!await hasNextPage(filter_tab)) break;
        filter = await nextPage(filter_tab);

        n++;
    }
    state.working = false;
    state.stopping = false;
    chrome.tabs.remove(filter_tab.id);
}

function getProfiles(tab) {
    return new Promise(resolve => {
        chrome.tabs.sendMessage(tab.id, { action: 'get_profiles' }, result => { resolve(result) });
    });
}

function getCampaigns() {
    return new Promise(resolve => {
        $.getJSON('http://localhost:3000/api/campaigns', {}, (data) => {
            state.online = true;
            resolve(data);
        }).fail(() => {
            state.online = false;
            console.log('offline');
        })
    });
}

function getCampaign(id) {
    return new Promise(resolve => {
        $.getJSON('http://localhost:3000/api/campaign/' + id, {}, (data) => {
            resolve(data[0]);
        });
    });
}

function hasNextPage(tab) {
    return new Promise(resolve => {
        chrome.tabs.sendMessage(tab.id, { action: 'has_next_page' }, result => { resolve(result) });
    });
}

function nextPage(tab) {
    return new Promise(resolve => {
        chrome.tabs.sendMessage(tab.id, { action: 'next_page' }, result => { resolve(result) });
    });
}

function sendConnect(tab, invite_message) {
    return new Promise(resolve => {
        chrome.tabs.sendMessage(tab.id, { action: 'send_connect', invite_message: invite_message }, result => {
            resolve(result)
        });
    });
}

async function sendConnectScript(profile, invite_message) {
    let tab = await createTab(profile.url);
    let result = await sendConnect(tab, invite_message);
    if (result) {
        // save connect
        await saveConnect(state.campaignId, profile.title, profile.url);
    }
    if (state.stopping) {
        chrome.tabs.remove(tab.id);
        return true;
    }
    await timeout(3000);
    chrome.tabs.remove(tab.id);
}

function saveConnect(campaign_id, lead_title, lead_url) {
    return new Promise(resolve => {
        $.post('http://localhost:3000/api/connects',
            { campaign_id: campaign_id,
              lead_title: lead_title,
              lead_url: lead_url }, (result) => {
            resolve(result);
        });
    });
}

function getConnects(campaign_id) {
    return new Promise((resolve, reject) => {
        $.getJSON('http://localhost:3000/api/connects?campaign_id=' + campaign_id,
            (connects) => {
                console.log('http://localhost:3000/api/connects = ' + connects);
                resolve(connects);
            }).fail(() => {
                reject();
            });
    });
}

function timeout(ms) {
    return new Promise(resolve => {
        setTimeout(() => { resolve() }, ms);
    });
}