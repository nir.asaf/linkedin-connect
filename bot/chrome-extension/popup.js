/**
 * Created by Oleg Vlasov on 06.07.17.
 */

function showOnlineView(online) {
    document.querySelector('div#view1').style.display = (online ? 'block' : 'none');
    document.querySelector('div#view2').style.display = (online ? 'none' : 'block');
}

document.addEventListener('DOMContentLoaded', function () {
    let btn_action = document.querySelector('button#action');
    btn_action.addEventListener('click', function(){
        chrome.runtime.sendMessage({action: 'toggle'});
    });
    chrome.runtime.sendMessage({action: 'get_campaigns'});
});

chrome.runtime.onMessage.addListener(message => {

    if (message.state) {

        showOnlineView(message.state.online);

        if (message.state.campaignId !== 0) {
            document.querySelector('#connects').innerText = 'Connects - ' + message.state.connects;
        } else {
            document.querySelector('#connects').innerHTML = '&nbsp;';
        }

        let btn_action = document.querySelector('button#action');
        if (message.state.working) {
            btn_action.innerText = 'STOP';
            btn_action.setAttribute('class', 'btn btn-danger');
        } else {
            btn_action.innerText = 'START';
            btn_action.setAttribute('class', 'btn btn-success');
        }

        document.querySelector('button#action').disabled = (message.state.stopping || message.state.campaignId === 0);
        document.querySelector('select#campaign').value = message.state.campaignId;
    }

    if (message.campaigns && message.state) {

        let select = document.querySelector('select#campaign');
        select.innerHTML = '<option value="0" selected>select campaign...</option>';

        for (let i in message.campaigns) {
            let option = document.createElement('option');
            option.value = message.campaigns[i].id;
            option.name = message.campaigns[i].name;
            option.innerText = message.campaigns[i].name;
            select.appendChild(option);
        }

        if (message.state.campaignId > 0) {  //} == message.campaigns[i].id) {
            select.value = message.state.campaignId;
        }
    }
});

document.querySelector('select#campaign').addEventListener('change', (ev) => {
    if (ev.currentTarget) {
        chrome.runtime.sendMessage({action: 'set_campaign', id: parseInt(ev.currentTarget.value)});
    }
});

chrome.runtime.sendMessage({action: 'get_state'});
