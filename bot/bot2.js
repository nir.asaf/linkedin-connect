/**
 * Created by Oleg Vlasov on 03.07.17.
 */

const webdriver = require('selenium-webdriver'),
      By = webdriver.By,
      Q = require('q'),
      credentials = require('./credentials.json');

var chromeCapabilities=webdriver.Capabilities.chrome()
var chromeOptions = {
        'args': ['--disable-infobars']
};
chromeCapabilities.set('chromeOptions', chromeOptions);

let driver = new webdriver.Builder()
      .forBrowser('chrome')
      .withCapabilities(chromeCapabilities)
      .build();

Q.spawn(function*() {
    try {

        let profile_urls = [];

        yield driver.get('https://www.linkedin.com');

        let login_email = yield driver.findElement(By.css('input#login-email')),
            login_password = yield driver.findElement(By.css('input#login-password')),
            login_submit = yield driver.findElement(By.css('input#login-submit'));

        // authentication
        yield login_email.sendKeys(credentials.username);
        yield login_password.sendKeys(credentials.password);
        yield login_submit.click();

        yield driver.sleep(5000);

        // move to filter
        yield driver.get("https://www.linkedin.com/mynetwork/invite-connect/connections/");

        // collect profiles


        yield driver.sleep(1000);

        let profiles = yield driver.findElements(By.css('.mn-connections__list li'));

        for(let i = 0; i < profiles.length; i++) {
            profiles[i].click()
            yield driver.sleep(1000);
            let name = yield driver.findElement(By.css('.pv-top-card-section__information h1'))
            console.log(name);
            driver.navigate().back();
            yield driver.sleep(1000);
            // let profile_url = yield profiles[i].getAttribute('href');
            //
            // console.log('found profile ' + profile_url);
            // profile_urls.push(profile_url);
        }




        // send connects
        // for(let i = 0; i < profile_urls.length; i++) {
        //
        //     console.log('send connect to ' + profile_urls[i]);
        //
        //     yield driver.get(profile_urls[i]);
        //
        //     yield driver.executeScript('document.querySelector("button.connect-button").click();');
        //     yield driver.sleep(5000);
        //
        //     let connect_message = yield driver.findElement(By.css('textarea#connect-message-content'));
        //     yield connect_message.clear();
        //     yield connect_message.sendKeys('It\'s sample connect message \n\n Regards, Bot');
        //
        //     let connect_submit = yield driver.findElement(By.css('form.connect-form button.submit-button')),
        //         connect_cancel = yield driver.findElement(By.css('form.connect-form button.cancel-button'));
        //
        //     // only mark "Send Invitation" button by red color
        //     yield driver.executeScript('document.querySelector("form.connect-form button.submit-button").style.color = "red";');
        //     yield driver.sleep(2000);
        //
        //     yield connect_cancel.click();
        //     yield driver.switchTo().alert().accept();
        //
        //     console.log('success');
        // }
        //
        // yield driver.sleep(5000);
        // console.log('exit');
        // driver.quit();

    } catch (err) {
        console.error(err)
    }
});
