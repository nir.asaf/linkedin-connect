bcrypt = require('bcrypt-nodejs');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_type: {
      type: DataTypes.STRING,
      defaultValue: "free"
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    password: DataTypes.STRING,
    passwordResetToken: DataTypes.STRING,
    passwordResetExpiresAt: DataTypes.DATE,
    
  }, {
    classMethods: {
      associate: function(models) {
          this.hasMany(models.Campaign, {as: "campaigns"});
      }
    },
    instanceMethods:{
        /**
        * Helper method for validating user's password.
        */
      comparePassword: function(candidatePassword,cb){
        cb(null, this.password == candidatePassword)
      }

      // function(candidatePassword, cb) {
      //    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      //     console.log(this.password, candidatePassword, isMatch);
      //     cb(err, isMatch);
      //   });
      // }
    }
  });



  
  return User;
};