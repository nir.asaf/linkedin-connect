'use strict';
module.exports = function(sequelize, DataTypes) {
  var Connect = sequelize.define('Connect', {
    CampaignId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    lead_title: DataTypes.STRING,
    lead_url: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Campaign);
      }
    }
  });
  return Connect;
};