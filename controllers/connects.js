const Connect = require('../models/index').Connect;
const Campaign = require('../models/index').Campaign;

// GET /api/connects?campaign_id=123
exports.getConnects = (req, res) => {
	Campaign.findById(req.query.campaign_id).then(campaign => {
		if(!campaign)
			res.json({ msg: "Campaign Not found" });
		else
		{
			campaign.getConnects().then(connects => {
				res.json(connects);
			}).catch(err => {
				res.status(500);
				res.json(err);
			});
		}
			
	}).catch(error => {
		res.status(500);
        res.json({error: err});
	});
};


// POST /api/connects
// Payload {campaign_id, lead_title, lead_url}
// Returns {connect_id}
exports.createConnect = (req, res) => {
	connect = Connect.build({
		CampaignId: req.body.campaign_id,
		lead_title: req.body.lead_title,
		lead_url: req.body.lead_url
	})

	// Create a new campaign
    connect.save().then(connect => {
    	res.json(connect)
    })
    .catch(err => {
        res.status(500);
        res.json({error: err});
    })

};

