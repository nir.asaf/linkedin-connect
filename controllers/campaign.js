const Campaign = require('../models/index').Campaign;

// GET /campaigns
exports.getCampaigns = (req, res) => {
	req.user.getCampaigns().then(campaigns => {
		return res.json(campaigns);
	}).catch(err => {
		res.status(500);
		return res.json(err);
	});
};


// GET /campaign/:id
exports.getSingleCampaign = (req, res) => {
	req.user.getCampaigns({where: {id: req.params.id}}).then(campaigns => {
	    res.json(campaigns);
  	}).catch(err => {
  		res.status(404);
  		return res.json({error: "Campaign not found"})
  	});
};

// POST /campaigns
exports.createCampaign = (req, res) => {
	campaign = Campaign.build({
		name: req.body.campaign_name,
		description: req.body.description,
		invite_message: req.body.invite_message,
    	search_filters: req.body.search_filters,
    	exclude_companies: req.body.exclude_companies,
    	UserId: req.user.id
	})

	// Create a new campaign
    campaign.save().then(campaign => {
    	res.json(campaign)
    })
    .catch(error => {
        return error;
    })

};


// PUT /campaign/:id
exports.updateCampaign = (req, res) => {
	Campaign.findById(req.id).then(campaign => {
		if(!campaign)
			res.json({ msg: "Not found" })

		// Update the campaign
		campaign.updateAttributes({
	        name: req.body.campaign_name,
			description: req.body.description,
			invite_message: req.body.invite_message,
	    	search_filters: req.body.search_filters,
	    	exclude_companies: req.body.exclude_companies,
	    }).then(campaign => {
	    	res.json(campaign);
	    });

	}).catch(error => {
		return next(error);
	});
};

// DELETE /campaign/:id
exports.deleteCampaign = (req, res) => {
	Campaign.destroy({ where: {id: req.id }}).then(campaign => {
		res.json(campaign);
	}).catch(error => {
		return next(error);
	});
};

// GET /campaigns/new
exports.newCampaign = (req, res) => {

};

// GET /campaign/:id/edit
exports.editCampaign = (req, res) => {

};
