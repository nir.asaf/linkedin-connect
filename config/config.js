module.exports = {
  development: {
    storage: "./db.development.sqlite",
    dialect: "sqlite",
    secret: "SuperSecretKey"
  },
  production: {
    username: "tpuxdrtwixbtly",
    password: "554c0da95debd354b070c5f6344f3351f783944b8de9ab31131f4778ad7faff1",
    database: "dep9cttbjjkrj3",
    host: "ec2-54-197-232-155.compute-1.amazonaws.com",
    dialect: "postgres"
  },
  staging: {
    url: "",
    dialect: 'postgres'
  },
  test: {
    username: "root",
    password: "",
    database: "database_test",
    host: "127.0.0.1",
    dialect: "mysql"
  }
};
