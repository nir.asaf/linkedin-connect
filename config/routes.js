module.exports = function(app) {

  /**
   * Controllers (route handlers).
   */
  const homeController = require('../controllers/home');
  const userController = require('../controllers/user');
  const contactController = require('../controllers/contact');
  const campaignsController = require('../controllers/campaign');
  const connectsController = require('../controllers/connects');

  /**
   * API keys and Passport configuration.
   */
  const passportConfig = require('../config/passport');
  const email          = require('../config/email');
  const passport = require('passport');

  isAuthenticated = passport.authenticate('jwt', { session: false });

  /**
   * Primary app routes.
   */
  app.get('/', homeController.index);
  app.get('/sendemail', email.sendEmail);

  // Account
  app.get('/loggedin', userController.isLoggedIn);
  app.post('/login', userController.postLogin);
  app.get('/logout', userController.logout);
  app.post('/forgot', userController.postForgot);
  app.post('/reset/:token', userController.postReset);
  app.post('/signup', userController.postSignup);
  app.post('/contact', contactController.postContact);
  app.post('/account/update', isAuthenticated, userController.postUpdateProfile);


  // Campaigns

  app.get('/api/campaigns', isAuthenticated, campaignsController.getCampaigns);
  app.get('/api/campaign/:id', isAuthenticated, campaignsController.getSingleCampaign);
  app.put('/api/campaign/:id', isAuthenticated, campaignsController.updateCampaign);
  app.post('/api/campaigns', isAuthenticated, campaignsController.createCampaign);
  app.get('/api/campaign/:id/edit', isAuthenticated, campaignsController.editCampaign);

  // Connects
  app.get("/api/connects", connectsController.getConnects);
  app.post("/api/connects", connectsController.createConnect);
};
